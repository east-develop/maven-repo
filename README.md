# README #

Maven repository for Eyefleet application. Its kept legacy libaraies which unable to find them in the Maven 's central repository.

## HOWTO use

Put this repository into your pom file.

```
<repositories>
...
    <repository>
        <id>eyefleet-mvn-releases</id>
        <url>https://bitbucket.org/api/1.0/repositories/east-develop/maven-repo/raw/repository</url>
    </repository>
...	
</repositories>
```

then your project will found those dependency in the repository.

## เพิ่ม library เข้าไปที่ repo นี้โดย 

```
mvn install:install-file -DgroupId=com.east.eyefleet -DartifactId=EyeLib -Dversion=1.0.0 -Dfile={/Users/siritas_s}/.m2/repository/com/east/eyefleet/eyelib/1.0.0/eyelib-1.0.0.jar -Dpackaging=jar -DpomFile={/Users/siritas_s}/.m2/repository/com/east/eyefleet/eyelib/1.0.0/eyelib-1.0.0.pom -DlocalRepositoryPath=. -DcreateChecksum=true
```

